﻿using Microsoft.AspNetCore.Mvc;
using Services.IService;
using Services.Models;
using Services.Service;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserInfoModel user)
        {
            var result = await _userService.CreateUser(user);
            return Ok(result);
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _userService.GetAllUsers();
            return Ok(result);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var result = await _userService.GetUserById(id);
            return Ok(result);
        }
        [HttpPut]
        public async Task<IActionResult> Put(string id, [FromBody] UserInfoModel user)
        {
            var result = await _userService.UpdateUser(id, user);
            return Ok(result);
        }
        [HttpDelete]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _userService.DeleteUser(id);
            return Ok(result);
        }
    }
}

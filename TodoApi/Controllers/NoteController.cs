﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Services.IService;
using Services.Models;
using Services.Service;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        private readonly INoteService _noteService;
        public NoteController(INoteService noteService)
        {
            _noteService = noteService;
        }
        [HttpPost]
        public async Task<IActionResult> Post(string userId, [FromBody] NoteModel note)
        {
            var result = await _noteService.CreateAsync(userId, note);
            return Ok(result);
        }
    }
}

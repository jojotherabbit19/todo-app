﻿using Microsoft.Extensions.DependencyInjection;
using Services.IService;
using Services.Service;

namespace Services
{
    public static class DependencyInjection
    {
        public static IServiceCollection CoreServices(this IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<INoteService, NoteService>();
            return services;
        }
    }
}

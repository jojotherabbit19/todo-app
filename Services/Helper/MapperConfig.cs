﻿using AutoMapper;
using Services.Entities;
using Services.Models;

public class MapperConfig : Profile
{
    public MapperConfig()
    {
        CreateMap<User, UserModel>().ReverseMap();
        CreateMap<Note, NoteModel>().ReverseMap();
        CreateMap<User, LoginModel>().ReverseMap();
        CreateMap<User, UserInfoModel>().ReverseMap();
    }
}
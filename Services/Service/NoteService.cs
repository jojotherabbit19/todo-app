﻿using AutoMapper;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using Services.Entities;
using Services.IService;
using Services.Models;

namespace Services.Service
{
    public class NoteService : INoteService
    {
        private readonly IMongoCollection<User> _userCollection;
        private readonly IMapper _mapper;
        public NoteService(IOptions<MongoDBSettings> mongoDBSettings, IMapper mapper)
        {
            MongoClient client = new MongoClient(mongoDBSettings.Value.ConnectionURI);
            IMongoDatabase database = client.GetDatabase(mongoDBSettings.Value.DatabaseName);
            _userCollection = database.GetCollection<User>(mongoDBSettings.Value.CollectionName);
            _mapper = mapper;
        }
        public async Task<NoteModel> CreateAsync(string objId, NoteModel model)
        {
            var note = _mapper.Map<Note>(model);
            // create new id for note (* should use int => find way to gen unique int)
            note.Id = Guid.NewGuid();
            // convert string to objectId
            ObjectId userId = new ObjectId(objId);
            
            var filter = Builders<User>.Filter.Eq("_id", userId);
            var update = Builders<User>.Update.AddToSet("Notes", note);

            var result = await _userCollection.UpdateOneAsync(filter, update);
            if (result.ModifiedCount > 0)
            {
                return model;
            }
            else
            {
                // Handle failure to update
                return null;
            }
        }

        public Task<bool> DeleteAsync(string objId, int id)
        {
            throw new NotImplementedException();
        }

        public Task<NoteModel> UpdateAsync(string objId, NoteModel model)
        {
            throw new NotImplementedException();
        }
    }
}

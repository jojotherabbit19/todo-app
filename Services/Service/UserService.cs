﻿using AutoMapper;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Services.Entities;
using Services.IService;
using Services.Models;

namespace Services.Service
{
    public class UserService : IUserService
    {
        private readonly IMongoCollection<User> _userCollection;
        private readonly IMapper _mapper;

        public UserService(IOptions<MongoDBSettings> mongoDBSettings, IMapper mapper)
        {
            MongoClient client = new MongoClient(mongoDBSettings.Value.ConnectionURI);
            IMongoDatabase database = client.GetDatabase(mongoDBSettings.Value.DatabaseName);
            _userCollection = database.GetCollection<User>(mongoDBSettings.Value.CollectionName);
            _mapper = mapper;
        }
        public async Task<UserInfoModel> CreateUser(UserInfoModel user)
        {
            var obj = _mapper.Map<User>(user);
            await _userCollection.InsertOneAsync(obj);
            return user;
        }

        public async Task<bool> DeleteUser(string userId)
        {
            var result = await _userCollection.DeleteOneAsync(x => x.Id == userId);
            if (result is null)
            {
                throw new Exception("error.delete");
            }
            return true;
        }

        public async Task<List<UserModel>> GetAllUsers()
        {
            return _mapper.Map<List<UserModel>>(await _userCollection.Find(x => true).ToListAsync());
        }

        public async Task<UserModel> GetUserById(string id)
        {
            var filter = Builders<User>.Filter.Eq(x => x.Id, id);
            var result = await _userCollection.Find(filter).FirstOrDefaultAsync();
            return _mapper.Map<UserModel>(result);
        }

        public async Task<UserInfoModel> UpdateUser(string userId, UserInfoModel user)
        {
            var obj = _mapper.Map<User>(user);
            var filter = Builders<User>.Filter.Eq(x => x.Id, userId);
            var update = Builders<User>.Update.Set(x => x.Email, user.Email)
                                              .Set(x => x.PasswordHash, user.PasswordHash)
                                              .Set(x => x.Name, user.Name);
            var result = await _userCollection.UpdateOneAsync(filter, update);
            if (result is null)
            {
                throw new Exception("error.update");
            }
            return user;
        }
    }
}

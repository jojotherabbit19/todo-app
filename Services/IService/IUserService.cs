﻿using MongoDB.Bson;
using Services.Models;

namespace Services.IService
{
    public interface IUserService
    {
        Task<UserInfoModel> CreateUser(UserInfoModel user);
        Task<UserInfoModel> UpdateUser(string userId, UserInfoModel user);
        Task<bool> DeleteUser(string userId);
        Task<UserModel> GetUserById(string id);
        Task<List<UserModel>> GetAllUsers();
    }
}

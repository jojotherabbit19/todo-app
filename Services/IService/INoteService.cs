﻿using Services.Models;

namespace Services.IService
{
    public interface INoteService
    {
        Task<NoteModel> CreateAsync(string objId, NoteModel model);
        Task<NoteModel> UpdateAsync(string objId, NoteModel model);
        Task<bool> DeleteAsync(string objId, int id);
    }
}

﻿namespace Services.Models
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string PasswordHash { get; set; }
    }
    public class UserInfoModel
    {
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string Name { get; set; }
    }
    public class UserModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public List<NoteModel> Notes { get; set; }
    }
    public class NoteModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsCompeleted { get; set; }
    }
}
